import { ADDITION, SUBTRACTION } from "./actionTypes";
 const initinlState ={
     counter : 0,
     name :' Cherucole',
};
export const mainReducer =( state = initinlState , action) =>{
    switch (action.type) {
        case ADDITION:
            
            return { ...state, counter: state.counter + 1};
        case SUBTRACTION :
            return { ...state , counter: state.counter -1 };
    
        default:
            return state;
    }
};